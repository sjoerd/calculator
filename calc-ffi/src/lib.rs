use calc::prelude::*;
use std::ffi::CStr;
use std::os::raw::c_char;
use std::panic::catch_unwind;
use std::ptr::null_mut;
use std::result::Result as StdResult;
use std::str::FromStr;

pub use calc::parse;
pub use calc::Expr;

#[repr(C)]
pub enum Result {
    Ok = 0,
    InvalidInput,
    UnexpectedInput,
    DivideByZero,
    InsufficientNumbers,
    EmptyStack,
    ExcessStack,
    Fatal = 0xff,
}

impl From<ParseError> for Result {
    fn from(e: ParseError) -> Self {
        match e {
            ParseError::UnexpectedInput(_) => Self::UnexpectedInput,
            ParseError::InsufficientNumbers => Self::InsufficientNumbers,
            ParseError::EmptyStack => Self::EmptyStack,
            ParseError::ExcessStack => Self::ExcessStack,
        }
    }
}

impl From<EvalError> for Result {
    fn from(e: EvalError) -> Self {
        match e {
            EvalError::DivideByZero => Self::DivideByZero,
        }
    }
}

fn r_parse_and_eval(expr: &str) -> StdResult<i64, Result> {
    let expr = Expr::from_str(expr)?;
    Ok(eval(&expr)?)
}

#[no_mangle]
pub extern "C" fn parse_and_eval(maybe_cstr: *const c_char, output: *mut i64) -> Result {
    catch_unwind(|| {
        if output.is_null() || output.is_null() {
            return Result::InvalidInput;
        }

        let s = match unsafe { CStr::from_ptr(maybe_cstr).to_str() } {
            Ok(c) => c,
            Err(_) => return Result::InvalidInput,
        };

        match r_parse_and_eval(s) {
            Ok(v) => {
                unsafe { *output = v };
                Result::Ok
            }
            Err(e) => e,
        }
    })
    .unwrap_or(Result::Fatal)
}

/// This will return null if unsuccessful
#[no_mangle]
pub extern "C" fn c_parse(maybe_cstr: *const c_char) -> *mut Expr {
    catch_unwind(|| {
        if maybe_cstr.is_null() {
            return null_mut();
        }

        let s = match unsafe { CStr::from_ptr(maybe_cstr).to_str() } {
            Ok(c) => c,
            Err(_) => return null_mut(),
        };

        match Expr::from_str(s) {
            Ok(e) => Box::into_raw(Box::new(e)),
            Err(_) => null_mut(),
        }
    })
    .unwrap_or(null_mut())
}

#[no_mangle]
pub extern "C" fn c_eval(expr: *const Expr, output: *mut i64) -> Result {
    catch_unwind(|| {
        // validate `expr` and `output`
        let expr = match unsafe { expr.as_ref() } {
            Some(e) => e,
            None => return Result::InvalidInput,
        };

        if output.is_null() {
            return Result::InvalidInput;
        }

        match eval(expr) {
            Ok(v) => {
                unsafe { *output = v };
                Result::Ok
            }
            Err(e) => e.into(),
        }
    })
    .unwrap_or(Result::Fatal)
}

/// # Safety
///
/// expr should be a valid pointer to a rust allocate Expr structure
#[no_mangle]
pub unsafe extern "C" fn release_expr(box_expr: *mut Expr) {
    if !box_expr.is_null() {
        Box::from_raw(box_expr);
    }
}
